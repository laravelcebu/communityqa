#Laravel(5.1) Base App + Angular


Please fork and contribute to enhance the structure. 

You can also suggest packages so that we can directly dive into code with this code base.

### Used Packages
- Laracast Flash
- HTML by Illuminate - for managing forms
- Repositories - yes, you don't need to write these.
- Api Guard - need an API on the fly? Cheers to these.
- Javascript to Php - by Laracasts
- Guzzle - we can't sent emails without this package.

### Bower Dependencies
- jQuery
- Bootstrap 3.*
- Font Awesome
- Angular
- Angular Bootstrap
- Angular Resource
- Angular Filter
- Moment


## INSTRUCTIONS
1. Make a database and update your ```.env``` file.
2. Run ```composer install``` to pull package dependencies.
3. Run ```php artisan migrate:install && php artisan migrate```
4. Run ```bower install``` to pull javascript libraries.
5. Use Laravel Elixir by running ```npm install```. Use ```sudo``` for super user.
6. Run ```gulp``` or ```gulp watch```.
7. Include ```public/css/all.css```, ```public/js/general.js``` and ```public/js/scripts.js``` on the templates you want to include the files.
8. Generate key ```php artisan key:generate``` for Laravel's encryption.
9. Finally run ```composer dump-autoload -o```, just to have a clean build.


## Versions

